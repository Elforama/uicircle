﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(UICircle))]
public class CircleHealthBar : MonoBehaviour {

	public int maxHealth = 10;

	private int currentHealth;

	public int maxAngle = 270;

	public bool smooth = true;

	private float anglePerHealth;

	public Text healthText;

	private UICircle circle;

	// Use this for initialization
	void Start () {
		circle = GetComponent<UICircle>();

		currentHealth = maxHealth;

		anglePerHealth = maxAngle / maxHealth;

		circle.Angle = maxAngle;

		healthText.text = currentHealth.ToString();
	}

	public int getHealth(){
		return currentHealth;
	}
	
	public void SubtractHealth(int amount){
		currentHealth -= amount;

		if(currentHealth < 0)
			currentHealth = 0;

		healthText.text = currentHealth.ToString();

		if(smooth)
			StartCoroutine("Smoothing");
		else
			circle.Angle = currentHealth * anglePerHealth;
	}

	public void AddHealth(int amount){
		if(currentHealth == maxHealth)
			return;

		currentHealth += amount;
		
		if(currentHealth > maxHealth)
			currentHealth = maxHealth;

		healthText.text = currentHealth.ToString();

		if(smooth)
			StartCoroutine("Smoothing");
		else
			circle.Angle = currentHealth * anglePerHealth;
	}

	IEnumerator Smoothing(){
		float start = circle.Angle;
		float end = currentHealth * anglePerHealth;
		while(start != end){
			start = Mathf.SmoothStep(start, end, .5f);
			circle.Angle = start;
			yield return new WaitForFixedUpdate();
		}
	}
}
