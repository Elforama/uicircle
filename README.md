# README #


### What is this repository for? ###

* This is a circle graphic that can be used within the Unity UI system.
   Features:
   * Gradient
   * Glow
   * Adjust glow size
   * Set donut hole in circle 
   * Draw circle up to a certain angle
   * Set the subdivisions for quality
   * Set the angle to start drawing the circle at
   * Draw circle in either positive or negative angle direction
   
VERSION: v1.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

* When glow is enabled it no uses the circles color instead of changing it to white.
* Fixed a bug where the circle would be drawn off center when the game object was moved while disabled and then enabled afterward.

### How do I get set up? ###

There are 2 files, UICircle, and UICircleInspector
Make sure UICircleInspector is in the Editor folder
Place UICircle onto a RectTransform GameObject to draw a circle

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Lawrence Muller 
* Other community or team contact